package ibucev.hr.notes;

/**
 * Created by ivanb on 1/13/2018.
 */

public class Note {
    private String id;
    private String title;
    private String description;
    private int priority;
    private boolean check;

    public Note(){

    }

    public Note(String id,String title, String description, int priority) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.priority = priority;
    }

    public Note(String id, String title, String description, int priority, boolean check) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.check = check;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}


