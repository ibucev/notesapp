package ibucev.hr.notes;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    private EditText editName, editSurname, editEmail, editPassword;
    private FirebaseAuth auth;
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("Users");
    private Button signUp, signIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editEmail = findViewById(R.id.editEmail);
        editPassword = findViewById(R.id.editPassword);
        editName = findViewById(R.id.editName);
        editSurname = findViewById(R.id.editSurname);

        signUp = findViewById(R.id.buttonSignUp);
        signIn = findViewById(R.id.buttonSignIn);
        auth = FirebaseAuth.getInstance();

        if (auth.getCurrentUser() != null) {
            Intent noteIntent = new Intent(MainActivity.this, ListNote.class);
            startActivity(noteIntent);
            finish();
        }

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = editEmail.getText().toString().trim();
                String password = editPassword.getText().toString().trim();
                String name = editName.getText().toString().trim();
                String surname = editSurname.getText().toString().trim();
                if(!email.isEmpty() && !password.isEmpty() && !name.isEmpty() && !surname.isEmpty()) {
                    singupUserWithEmailAndPassword(email, password);
                } else {
                    Toast.makeText(getApplicationContext(), "Some fields are empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signIn = new Intent(getApplicationContext(), SignIn.class);
                startActivity(signIn);
                finish();
            }
        });
    }

    private void singupUserWithEmailAndPassword(String email, String password) {
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (!task.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_LONG).show();
                } else {
                    String id = auth.getCurrentUser().getUid();
                    String name = editName.getText().toString().trim();
                    String surname = editSurname.getText().toString().trim();
                    String email = editEmail.getText().toString().trim();

                    User user = new User(id, name, surname, email);

                    myRef.child(id).setValue(user);
                    userProfile();
                }
            }
        });
    }

    private void userProfile() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (firebaseUser != null) {
            UserProfileChangeRequest userProfileChangeRequest = new UserProfileChangeRequest.Builder().setDisplayName(editName.getText().toString().trim()).build();
            firebaseUser.updateProfile(userProfileChangeRequest).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    if (task.isSuccessful()) {
                        Intent intent = new Intent(getApplicationContext(), ListNote.class);
                        startActivity(intent);
                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
}
