package ibucev.hr.notes;



import java.util.HashMap;
import java.util.Map;


/**
 * Created by ivanb on 1/13/2018.
 */

public class User {

    Map<Integer, String> users;
    private String id;
    private String name;
    private String surname;
    private String email;

    public  User() {

    }

    public User(String id, String name, String surname, String email) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.users = new HashMap<>();
    }

    public User(Map<Integer, String> users) {
        this.users = users;
    }

    public Map<Integer, String> getUsers() {
        return users;
    }

    public void setUsers(Map<Integer, String> users) {
        this.users = users;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void addUser() {

    }
}
