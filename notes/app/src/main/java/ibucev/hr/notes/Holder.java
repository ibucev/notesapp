package ibucev.hr.notes;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created by ivanb on 1/29/2018.
 */

public class Holder extends RecyclerView.ViewHolder{

    TextView noteTitle;
    TextView noteDescription;
    TextView notePriority;
    CheckBox noteCheck;

    View view;

    public Holder(View itemView) {
        super(itemView);

        noteTitle = itemView.findViewById(R.id.textTitle);
        noteDescription = itemView.findViewById(R.id.textDescription);
        notePriority = itemView.findViewById(R.id.textPriority);
        noteCheck = itemView.findViewById(R.id.checkNote);

        view = itemView;
    }
}
