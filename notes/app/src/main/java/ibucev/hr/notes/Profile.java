package ibucev.hr.notes;

import android.content.Intent;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Profile extends AppCompatActivity {

    FirebaseAuth auth;
    ImageView backButton;
    Button logoutButton;
    TextView mail, doneNotes, todoNotes;
    DatabaseReference databaseReference;
    long numberDone = 0;
    long numberTodo = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        backButton = findViewById(R.id.backArrow);
        logoutButton = findViewById(R.id.logOut);
        mail = findViewById(R.id.email);
        todoNotes = findViewById(R.id.todoNumber);
        doneNotes = findViewById(R.id.doneNumber);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ListNote.class));
                finish();
            }
        });

        auth = FirebaseAuth.getInstance();
        mail.setText(auth.getCurrentUser().getEmail());

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                auth.signOut();
                startActivity(new Intent(getApplicationContext(), SignIn.class));
                finish();
            }
        });

        databaseReference = FirebaseDatabase.getInstance().getReferenceFromUrl("https://notes-8077d.firebaseio.com/Users/VxuH1ADPgbM37lxp9AGgHQEJu6H2/Messages/");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Note note = child.getValue(Note.class);

                    if (note.isCheck() == true) {
                        numberDone++;
                        doneNotes.setText(numberDone + "");
                    } else {
                        numberTodo++;
                        todoNotes.setText(numberTodo + "");
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });






    }
}
