package ibucev.hr.notes;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * Created by ivanb on 1/19/2018.
 *
 * Methods to manipulate with notes.
 */

public class ManageNotes {

    private DatabaseReference databaseReference;

    public Intent editNote(Context context, String id, String user){
        Intent note = new Intent(context, EditNote.class);
        note.putExtra("NOTE_ID", id);
        note.putExtra("USER_ID", user);
        return note;
    }

    public void deleteNote(final DatabaseReference myRef, final AlertDialog dialog, View view) {
        Button buttonDelete = view.findViewById(R.id.buttonDelete);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myRef.removeValue();
                dialog.dismiss();
            }
        });
    }

    public void deleteAllNotes(DatabaseReference databaseReference){
        databaseReference.removeValue();
    }

    public void checkAll(final DatabaseReference myRef){
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String id = snapshot.getKey();
                    try {
                        String currentUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        databaseReference = FirebaseDatabase.getInstance().getReferenceFromUrl("https://notes-8077d.firebaseio.com/Users/"
                                + currentUser + "/" + "Messages/" + id + "/");
                        databaseReference.child("check").setValue(true);
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

}
