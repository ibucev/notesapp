package ibucev.hr.notes;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class CreateNote extends AppCompatActivity {

    DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("Users");

    FirebaseAuth auth;
    Toolbar toolbar;

    private EditText editTitle, editDescription, editPriority;
    private Button saveNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create Note");
        toolbar.setTitleTextColor(Color.WHITE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        auth = FirebaseAuth.getInstance();
        editTitle = findViewById(R.id.editTitle);
        editDescription = findViewById(R.id.editDescription);
        editPriority = findViewById(R.id.editPriority);
        saveNote = findViewById(R.id.buttonSaveNote);


        if (auth.getCurrentUser() == null) {

            Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(mainActivityIntent);
            finish();

        }

        saveNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String noteId = myRef.push().getKey();
                String title = editTitle.getText().toString().trim();
                String description = editDescription.getText().toString().trim();
                int priority;

                try {
                    priority = Integer.parseInt(editPriority.getText().toString().trim());
                } catch (NumberFormatException e){
                    priority = 0;
                }

                if (!title.isEmpty() && !description.isEmpty()) {
                    Note note = new Note(noteId, title, description, priority, false);
                    try {
                        myRef.child(auth.getCurrentUser().getUid()).child("Messages").child(noteId).setValue(note);
                    } catch (Exception e) {
                        e.getMessage();
                    }
                    clearFields(editTitle, editDescription, editPriority);
                    Intent newIntent = new Intent(getApplicationContext(), ListNote.class);
                    startActivity(newIntent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Neka polja su prazna", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void clearFields(EditText title, EditText description, EditText priority) {
        title.getText().clear();
        description.getText().clear();
        priority.getText().clear();
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(getApplicationContext(), ListNote.class);
        startActivity(intent);
        finish();
        return true;
    }

}
