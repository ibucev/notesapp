package ibucev.hr.notes;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ListNote extends AppCompatActivity {

    private static final String TAG = "";
    private DatabaseReference databaseReference;
    private FirebaseAuth auth;
    private FirebaseRecyclerAdapter<Note, Holder> firebaseRecyclerAdapter;
    private RecyclerView recyclerView;
    private FloatingActionButton floatingAdd;
    private Button buttonYes, buttonNo;
    ManageNotes manageNotes = new ManageNotes();
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_note);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Notes");
        toolbar.setTitleTextColor(Color.WHITE);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        floatingAdd = findViewById(R.id.floatingActionButton2);

        try {
            auth = FirebaseAuth.getInstance();
            final String currentUser = auth.getCurrentUser().getUid();
            databaseReference = FirebaseDatabase.getInstance().getReferenceFromUrl("https://notes-8077d.firebaseio.com/Users/" + currentUser + "/" + "Messages/");
        } catch (Exception e) {
            e.getMessage();
        }

        ValueEventListener noteListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getValue(Note.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "Database error: ", databaseError.toException());
            }
        };

        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Note, Holder>(Note.class, R.layout.custom_row, Holder.class, databaseReference) {
            @Override
            protected void populateViewHolder(final Holder viewHolder, final Note model, final int position) {
                final DatabaseReference myRef = firebaseRecyclerAdapter.getRef(position);

                viewHolder.noteTitle.setText(model.getTitle());
                viewHolder.noteDescription.setText(model.getDescription());
                viewHolder.notePriority.setText(model.getPriority() + "");
                viewHolder.noteCheck.setChecked(model.isCheck());

                if(viewHolder.noteCheck.isChecked()){
                    viewHolder.noteTitle.setPaintFlags(viewHolder.noteTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    viewHolder.noteDescription.setPaintFlags(viewHolder.noteDescription.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                } else {
                    viewHolder.noteTitle.setPaintFlags(0);
                    viewHolder.noteDescription.setPaintFlags(0);
                }

                viewHolder.noteCheck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(viewHolder.noteCheck.isChecked()){
                            viewHolder.noteCheck.setChecked(true);
                            viewHolder.noteTitle.setPaintFlags(viewHolder.noteTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            viewHolder.noteDescription.setPaintFlags(viewHolder.noteDescription.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                            myRef.child("check").setValue(viewHolder.noteCheck.isChecked());
                        } else if(!viewHolder.noteCheck.isChecked()){
                            viewHolder.noteCheck.setChecked(false);
                            viewHolder.noteTitle.setPaintFlags(0);
                            viewHolder.noteDescription.setPaintFlags(0);

                            myRef.child("check").setValue(viewHolder.noteCheck.isChecked());
                        }
                    }
                });

                viewHolder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String id = model.getId();
                        String user = auth.getCurrentUser().getUid();

                        startActivity(manageNotes.editNote(getApplicationContext(), id, user));
                        finish();
                    }
                });

                viewHolder.view.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        final DatabaseReference myRef = firebaseRecyclerAdapter.getRef(position);
                        AlertDialog.Builder builder = new AlertDialog.Builder(ListNote.this);
                        view = getLayoutInflater().inflate(R.layout.dialog_action, null);

                        builder.setView(view);
                        final AlertDialog dialog = builder.create();
                        dialog.show();

                        manageNotes.deleteNote(myRef, dialog, view);
                        return true;
                    }
                });

            }
        };

        floatingAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addNote = new Intent(getApplicationContext(), CreateNote.class);
                startActivity(addNote);
                finish();
            }
        });

        recyclerView.setAdapter(firebaseRecyclerAdapter);
        databaseReference.addValueEventListener(noteListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.deleteAll:
                AlertDialog.Builder builder = new AlertDialog.Builder(ListNote.this);
                View view = getLayoutInflater().inflate(R.layout.alert_delete_all, null);
                builder.setView(view);
                final AlertDialog dialog = builder.create();
                dialog.show();

                buttonYes = view.findViewById(R.id.buttonYes);
                buttonYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        manageNotes.deleteAllNotes(databaseReference);
                        dialog.dismiss();
                    }
                });
                buttonNo = view.findViewById(R.id.buttonNo);
                buttonNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                break;
            case R.id.checkAll:
                manageNotes.checkAll(databaseReference);
                break;
            case R.id.profile:
                startActivity(new Intent(getApplicationContext(),Profile.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
