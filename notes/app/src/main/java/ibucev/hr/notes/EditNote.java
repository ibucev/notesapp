package ibucev.hr.notes;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class EditNote extends AppCompatActivity {

    EditText noteTitle, noteDescription, notePriority;
    boolean check;
    Button buttonSaveNote;
    Toolbar toolbar;

    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Edit Note");
        toolbar.setTitleTextColor(Color.WHITE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        noteTitle = findViewById(R.id.editTitle);
        noteDescription = findViewById(R.id.editDescription);
        notePriority = findViewById(R.id.editPriority);
        buttonSaveNote = findViewById(R.id.buttonSaveNote);
        check = false;

        String currentUser = getIntent().getStringExtra("USER_ID");
        final String noteId = getIntent().getStringExtra("NOTE_ID");
        databaseReference = FirebaseDatabase.getInstance().getReferenceFromUrl("https://notes-8077d.firebaseio.com/Users/"
                + currentUser + "/" + "Messages/" + noteId + "/");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Note note = dataSnapshot.getValue(Note.class);

                if (note != null) {
                    noteTitle.setText(note.getTitle());
                    noteTitle.setSelection(noteTitle.getText().toString().length());
                    noteDescription.setText(note.getDescription());
                    notePriority.setText(note.getPriority() + "");
                    check = note.isCheck();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        buttonSaveNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNote(noteId);
            }
        });
    }

    private void saveNote(String noteId) {
        String title = noteTitle.getText().toString().trim();
        String description = noteDescription.getText().toString().trim();
        boolean done = check;
        int priority;

        try {
            priority = Integer.parseInt(notePriority.getText().toString().trim());
        } catch (NumberFormatException e) {
            priority = 0;
        }

        if (!title.isEmpty() && !description.isEmpty()) {
            Note note = new Note(noteId, title, description, priority, done);
            try {
                databaseReference.setValue(note);
            } catch (Exception e) {
                e.getMessage();
            }
            Intent newIntent = new Intent(getApplicationContext(), ListNote.class);
            startActivity(newIntent);
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Neka polja su prazna", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(getApplicationContext(), ListNote.class);
        startActivity(intent);
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.delete_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.deleteNote) {
            databaseReference.removeValue();
            onSupportNavigateUp();
        }
        return super.onOptionsItemSelected(item);
    }

}
